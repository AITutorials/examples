![Logo](http://www.tisv.cn/img/logo.png)

--------------------------------------------------------------------------------


[![Build Status](http://www.tisv.cn/img/badge.svg)](http://www.tisv.cn/) [![GitHub stars](http://www.tisv.cn/img/givemeastar.png)](https://github.com/AITutorials/)


## 案例说明

我们将为您提供并更新有关AI的教程案例, 这些案例或许适用于企业或机构教学环境, 它们带有完整的真实数据, 具有对比实验的多种模型方案, 明确的结果和结论以及详细的代码注释说明。


---

## 经典案例

#### 使用RNN模型构建人名分类器

以一个人名为输入, 使用模型帮助我们判断它最有可能是来自哪一个国家的人名, 这在某些国际化公司的业务中具有重要意义, 在用户注册过程中, 会根据用户填写的名字直接给他分配可能的国家或地区选项, 以及该国家或地区的国旗, 限制手机号码位数等等。		

**[点击查看](http://www.aitutorials.cn:8002/2/#21-rnn)**

---


#### 基于RNN的架构解析

RNN(Recurrent Neural Network), 中文称作循环神经网络, 它一般以序列数据为输入, 通过网络内部的结构设计有效捕捉序列之间的关系特征, 一般也是以序列形式进行输出。
因为RNN结构能够很好利用序列之间的关系, 因此针对自然界具有连续性的输入序列, 如人类的语言, 语音等进行很好的处理, 广泛应用于NLP领域的各项任务, 如文本分类, 情感分析, 意图识别, 机器翻译等。		

**[点击查看](http://www.aitutorials.cn:8002/1/)**


---

#### 使用seq2seq模型架构实现英译法任务

seq2seq模型架构, 包括两部分分别是encoder(编码器)和decoder(解码器), 编码器和解码器的内部实现都使用了GRU模型, 并带有Attention机制。		

**[点击查看](http://www.aitutorials.cn:8002/2/#22-seq2seq)**

---

## 其他资源

* [Dataset(集成世界范围内重要AI技术解决方案)](https://github.com/AITutorials/dataset)

* [Solutions(集成世界范围内重要AI技术解决方案)](https://github.com/AITutorials/solutions)

* [Cooperation(寻求更多资源与合作伙伴)](https://github.com/AITutorials/cooperation)

---
